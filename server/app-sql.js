//import libs
const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const cors = require('cors');

//configure the application
const mysqlConfig = require('./mysql-config');
const pool = mysql.createPool(mysqlConfig);

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

const mkQuery = (SQL, pool) => {
    return (params) => {
        const p = new Promise((resolve, reject) => {
            console.log("In closure: ", SQL);
            pool.getConnection((err, conn) => {
                if (err) {
                    reject({status: 500, error: err}); return;
                }
                conn.query(SQL, params || [],
                    (err, result) => {
                        try {
                            if (err)
                                reject(err);
                            else
                                reject({status: 400, error: err}); return;
                        } finally {
                            conn.release();
                        }
                    }
                )
            })
        })
        return (p);
    }
}

/*
a. GET members/
    -   returns list of members
b. GET members/{id}
    -   returns details of a specific member
c. POST members/
    -   create a new member
d. PUT members/{id}
    -   updates the details of a specific member
e. DELECT members/{id}
    -   deletes a specific member
*/

//a. GET members/ - returns list of members
const SELECT_ALL_MEMBERS = 'select id, email, password, last_name, first_name from members';
const selectAllMembers = mkQuery(SELECT_ALL_MEMBERS, pool);
app.get('/members', (req, resp) => {
    const limit = req.query.limit|| 20; 
    const offset = req.query.offset|| 0; 

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(SELECT_ALL_MEMBERS, [ parseInt(limit), parseInt(offset) ],
           (error, results) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); return;
                   }
                   resp.status(200).json(results);
               } finally {
                   conn.release();
               }
           }
       )
   });
});

//b. GET members/{id} - returns details of a specific member
const SELECT_MEMBER_BY_ID = 'select * from members where id = ?';
const selectMemberById = mkQuery(SELECT_MEMBER_BY_ID, pool);
app.get('/members/:id', (req, resp) => {
    console.log(`member id = ${req.params.id}`);

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(SELECT_MEMBER_BY_ID, [ req.params.id],
           (error, result) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); 
                       return;
                   }
                   if (result.length)
                       resp.status(200).json(result[0]);
                   else
                       resp.status(404).json({error: 'Details Not Found'});
               } finally { conn.release() ;}
           }
       )
   })
});


//c. POST members/ - create a new member
const INSERT_ONE_MEMBER = 'insert into members (email, password, last_name, first_name) values (?, ?, ?, ?)';
const insertOneMember = mkQuery(INSERT_ONE_MEMBER, pool);
app.post('/members', (req, resp) => {
    console.log(req.body);

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(INSERT_ONE_MEMBER, [ req.body.email, req.body.password, req.body.last_name, req.body.first_name ],
           (error, result) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); 
                       return;
                   }
                   if (result.length)
                       resp.status(200).json(result[0]);
                   else
                       resp.status(404).json({error: 'Not Created'});
               } finally { conn.release() ;}
           }
       )
   })
});


//d. PUT members/{id} - updates the details of a specific member
const UPDATE_ONE_MEMBER = 'update members set email=?, password=?, last_name=?, first_name=? where id=?';
const updateOneMember = mkQuery(UPDATE_ONE_MEMBER, pool);

app.put('/members', (req, resp) => {
    console.log(req.body);

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(UPDATE_ONE_MEMBER, [ req.body.email, req.body.password, req.body.last_name, req.body.first_name, req.body.id ],
           (error, result) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); 
                       return;
                   }
                   if (result.length)
                       resp.status(200).json(result[0]);
                   else
                       resp.status(404).json({error: 'Not Updated'});
               } finally { conn.release() ;}
           }
       )
   })
});


//e. DELETE members/{id} - deletes a specific member
const DELETE_MEMBER_BY_ID = 'delete from members where id = ?';
const deleteOneMember = mkQuery(DELETE_MEMBER_BY_ID, pool);
app.delete('/members/:id', (req, resp) => {
    console.log(`member id = ${req.params.id}`);

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(DELETE_MEMBER_BY_ID, [ req.params.id],
           (error, result) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); 
                       return;
                   }
                   if (result.length)
                       resp.status(200).json(result[0]);
                   else
                       resp.status(404).json({error: 'Not Deleted'});
               } finally { conn.release() ;}
           }
       )
   })
});



//start the application
pool.getConnection((err, conn) => {
    if (err) {
        console.error('>> error: ', err);
        process.exit(-1);
    }

    try {
        conn.ping((err) => {
            if (err) {
                console.error('>> ping error: ', err);
                process.exit(-1);
            }
            app.listen(3000, () => {
                console.log('Application started at port %d', 3000);
            });
        })

    } finally {
        conn.release();
    }
})
