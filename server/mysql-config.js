const CONFIG = {
    host: 'localhost', port: 3306,
    user: 'rong', password: 'rongrong',
    database: 'baking_recipe',
    connectionLimit: 5
};

module.exports = CONFIG;
