//require('dotenv').config()

var express = require("express");
var bodyParser = require("body-parser");
var mysql = require("mysql");
var q = require("q");
var cors = require('cors');

const dbConfig = require('./mysql-config');
const pool = mysql.createPool(dbConfig);

//load config
//const mysqlConfig = require('./mysql-config');
//const pool = mysql.createPool(mysqlConfig);

var app = express();

app.use(cors())
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

/*
var pool = mysql.createPool({
    host: process.env.MYSQL_SERVER,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    connectionLimit: process.env.MYSQL_CONNECTION
});
*/

//const findAllBooks = "SELECT id, title, cover_thumbnail, author_firstname, author_lastname FROM books LIMIT ? OFFSET ?";
//const findOneBook = "SELECT * FROM books WHERE id = ?";
//const updateBook = "UPDATE books SET title = ?, author_firstname = ?, author_lastname = ? WHERE id = ?";
//const saveOneBook = "INSERT INTO books (title, cover_thumbnail, author_firstname, author_lastname) VALUES (? ,? ,? ,?)";
const saveOneRecipe = "INSERT INTO recipe (name, description, category, instructions) VALUES (? ,? ,?, ?)";
const findByCategoryRecipe = "SELECT id, name, description, category, instructions FROM recipe WHERE category = ?";
const findAllRecipes = "SELECT id, name, description, category, instructions FROM recipe LIMIT ? OFFSET ?";
//const searchBooksByCriteria = "SELECT * FROM books WHERE (title LIKE ?) || author_firstname LIKE ? || author_lastname LIKE ?";
//const searchBookByTitle = "SELECT * FROM books WHERE title LIKE ?";
//const searchBookByName = "SELECT * FROM books WHERE author_firstname LIKE ? || author_lastname LIKE ?";
//const NODE_PORT = process.env.PORT;

var makeQuery = function (sql, pool) {
    console.log(sql);
    return function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(results);
            });
        });
        return defer.promise;
    }
};


//var findAll = makeQuery(findAllBooks, pool);
//var findOne = makeQuery(findOneBook, pool);

//var updateOne = makeQuery(updateBook, pool);
//var saveOne = makeQuery(saveOneBook, pool);

var saveOne = makeQuery(saveOneRecipe, pool);
var findByCategory = makeQuery(findByCategory, pool);
var findAll = makeQuery(findAllRecipes, pool);

//var searchBooks = makeQuery(searchBooksByCriteria, pool);
//var searchByTitle = makeQuery(searchBookByTitle, pool);
//var searchByName = makeQuery(searchBookByName, pool);

/*
app.get("/api/books", function (req, res) {
    console.log(req.query.limit);
    console.log(req.query.offset);
    var limit = parseInt(req.query.limit) || 50;
    var offset = parseInt(req.query.offset) || 0;
    findAll([limit, offset])
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});
*/


app.get("/api/recipes", function (req, res) {
    //console.log(req.query.limit);
    //console.log(req.query.offset);
    var limit = parseInt(req.query.limit) || 50;
    var offset = parseInt(req.query.offset) || 0;
    findAll([limit, offset])
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.get("/api/recipes", function (req, res) {
    console.log(req.query.category);
    //console.log(req.query.limit);
    //console.log(req.query.offset);
    //var limit = parseInt(req.query.limit) || 50;
    //var offset = parseInt(req.query.offset) || 0;
    //findAll([limit, offset])
    var category = string(req.query.category);
    findByCategory([category])
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
    });
});

/*
app.get("/api/book/:bookId", function (req, res) {
    console.log(req.params.bookId);
    findOne([req.params.bookId])
        .then(function (results) {
            res.status(200).json(results[0]);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});
*/

/*
app.post("/api/books", function (req, res) {
    console.log(req.body);
    
    saveOne([req.body.book_title, req.body.imageUrl ,req.body.author_firstname, req.body.author_lastname])
        .then(function (result) {
            res.status(200).json(result);
            console.log(result);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});
*/

app.post("/api/recipes", function (req, res) {
    console.log(req.body);
    //const id = ''
    //saveOne([id, req.body.name, req.body.description, req.body.category, req.body.instructions])
    
    saveOne([req.body.name, req.body.description, req.body.category, req.body.instructions])
        .then(function (result) {
            res.status(200).json(result);
            console.log(result);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

/*
app.put("/api/books", function (req, res) {
    console.log(req.body);
    updateOne([req.body.book_title, req.body.author_firstname, req.body.author_lastname, req.body.id])
        .then(function (result) {
            console.log(result);
            res.status(200).json(result);
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).end();
        });
});

app.get("/api/books/search", function (req, res) {
    console.log(req.query);
    var searchType = req.query.searchType;
    var keyword = req.query.keyword;
    if(typeof searchType === 'string' && searchType != '1') {
        if(searchType=='Title'){
            console.log('search by title');
            var title = "%" + keyword + "%";
            searchByTitle([title])
                .then(function (results) {
                    res.status(200).json(results);
                    //console.log(results);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).end();
                });
        }
        else {
            console.log('search by author');
            var authorName = keyword.split(' ');
            if(!authorName[1]) authorName[1] = authorName[0];
            var firstname = "%" + authorName[0] + "%";
            var lastname = "%" + authorName[1] + "%";
                searchByName([firstname, lastname])
                .then(function (results) {
                    res.status(200).json(results);
                    //console.log(results);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).end();
                });
        }
    }
    else {
        console.log('search by both');
        var title = "%" + keyword + "%";
        var authorName = keyword.split(' ');
        if(!authorName[1]) authorName[1] = authorName[0];
        var firstname = "%" + authorName[0] + "%";
        var lastname = "%" + authorName[1] + "%";
        console.log(title);
        console.log(firstname);
        console.log(lastname);
        searchBooks([title, firstname, lastname])
            .then(function (results) {
                res.status(200).json(results);
                //console.log(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    }
});
*/


//start the application
/*
const PORT = process.argv[2] || process.env.APP_PORT || 3000;
app.listen(PORT, () => {
    console.log('Application started on port %d', PORT);
});
*/

//const NODE_PORT = process.env.PORT;

app.use(express.static(__dirname + "/public"));

//app.listen(NODE_PORT, function () {
//    console.info("App server started on port " + NODE_PORT);
//});

const PORT = 3000;

//Only start the application if the db is up
console.log('Pinging database...');
pool.getConnection((err, conn) => {
    if (err) {
        console.error('>Error: ', err);
        process.exit(-1);
    }

    conn.ping((err) => {
        if (err) {
            console.error('>Cannot ping: ', err);
            process.exit(-1);
        }
        conn.release();

        //Start application only if we can ping database
        app.listen(PORT, () => {
            console.log('Starting application on port %d', PORT);
        });
    });
});





//const GET_ALL_RECIPE = "SELECT id, name, description, instructions FROM recipe";
//const SAVE_ONE_RECIPE = "INSERT INTO recipe (name, description, instructions) VALUES (?, ?, ?)";

/*
var makeQuery = function(sql, pool) {
    console.log("SQL : " + sql);
    return function(args) {
        new Promise(function(esolve, reject) {
            ppol.getConnection(function(err, conn){
                if(err){
                    reject(err);
                    return;
                }

                conn.query(sql, args || [], (err, results)=>{
                    conn.release();
                    if(err){
                        reject(err);
                        return;
                    }
                    resolve(results);

                });
            });
        });
    }
};

//var findAll = makeQuery(GET_ALL_RECIPE, pool);
//var saveOne = makeQuery(SAVE_ONE_RECIPE, pool);

/*
app.get('/recipes', function(req, res){

    findAll().then((results)=>{
        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });

    //res.status(200).json({user: 'test'});

});
*/

/*
app.post('/recipes/recipe-add', function(req, res){
    console.log(req.body);
    var recipeBodystr = JSON.stringify(req.body);
    var recipeBody = JSON.parse(recipeBodystr);

    console.log("recipeBody.status " + recipebody.status);

    saveOne([recipeBody.user, recipeBody.status]).then((result)=>{
        console.log('result > ' + results);
        // results.values = rsvpBody;
        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
    })
});
*/
/*
app.post("/api/recipe/save", function (req, res) {
    console.log(req.body.params);
    saveOne([req.body.params.name, req.body.params.description, req.body.params.instructions])
        .then(function (results) {
            res.status(200).json(results);
            console.log(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});
*/

/*
    console.log(rsvpBody.user);
    console.log(rsvpBody.age);

    res.status(200).json({});

});
*/
/*
const NODE_PORT = process.env.PORT;

app.listen(NODE_PORT, function(){
    console.log("Express server started, Listening at " + NODE_PORT);

});



/*
const findAllBooks = "SELECT id, title, cover_thumbnail, author_firstname, author_lastname FROM books LIMIT ? OFFSET ?";
const findOneBook = "SELECT * FROM books WHERE id = ?";
const saveOneBook = "UPDATE books SET title = ?, author_firstname = ?, author_lastname = ? WHERE id = ?";
const searchBooksByCriteria = "SELECT * FROM books WHERE (title LIKE ?) || author_firstname LIKE ? || author_lastname LIKE ?";
const searchBookByTitle = "SELECT * FROM books WHERE title LIKE ?";
const searchBookByName = "SELECT * FROM books WHERE author_firstname LIKE ? || author_lastname LIKE ?";
const NODE_PORT = process.env.PORT;

var makeQuery = function (sql, pool) {
    console.log(sql);
    return function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(results);
            });
        });
        return defer.promise;
    }
};


var findAll = makeQuery(findAllBooks, pool);
var findOne = makeQuery(findOneBook, pool);
var saveOne = makeQuery(saveOneBook, pool);
var searchBooks = makeQuery(searchBooksByCriteria, pool);
var searchByTitle = makeQuery(searchBookByTitle, pool);
var searchByName = makeQuery(searchBookByName, pool);

app.get("/api/books", function (req, res) {
    console.log(req.query.limit);
    console.log(req.query.offset);
    var limit = parseInt(req.query.limit) || 50;
    var offset = parseInt(req.query.offset) || 0;
    findAll([limit, offset])
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.get("/api/book/:bookId", function (req, res) {
    console.log(req.params.bookId);
    findOne([req.params.bookId])
        .then(function (results) {
            res.status(200).json(results[0]);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.post("/api/recipe/save", function (req, res) {
    console.log(req.body.params);
    saveOne([req.body.params.name, req.body.params.description, req.body.params.instructions])
        .then(function (results) {
            res.status(200).json(results);
            console.log(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

/*
app.post("/api/book/save", function (req, res) {
    console.log(req.body.params);
    saveOne([req.body.params.title, req.body.params.author_firstname, req.body.params.author_lastname, req.body.params.id])
        .then(function (results) {
            res.status(200).json(results);
            console.log(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});
*/
/*
app.get("/api/books/search", function (req, res) {
    console.log(req.query);
    var searchType = req.query.searchType;
    var keyword = req.query.keyword;
    if(typeof searchType === 'string' && searchType != '1') {
        if(searchType=='Title'){
            console.log('search by title');
            var title = "%" + keyword + "%";
            searchByTitle([title])
                .then(function (results) {
                    res.status(200).json(results);
                    //console.log(results);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).end();
                });
        }
        else {
            console.log('search by author');
            var authorName = keyword.split(' ');
            if(!authorName[1]) authorName[1] = authorName[0];
            var firstname = "%" + authorName[0] + "%";
            var lastname = "%" + authorName[1] + "%";
                searchByName([firstname, lastname])
                .then(function (results) {
                    res.status(200).json(results);
                    //console.log(results);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).end();
                });
        }
    }
    else {
        console.log('search by both');
        var title = "%" + keyword + "%";
        var authorName = keyword.split(' ');
        if(!authorName[1]) authorName[1] = authorName[0];
        var firstname = "%" + authorName[0] + "%";
        var lastname = "%" + authorName[1] + "%";
        console.log(title);
        console.log(firstname);
        console.log(lastname);
        searchBooks([title, firstname, lastname])
            .then(function (results) {
                res.status(200).json(results);
                //console.log(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    }
});



app.use(express.static(__dirname + "/public"));

app.listen(NODE_PORT, function () {
    console.info("App server started on port " + NODE_PORT);
});
*/