// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCmYpkq9r06VzeG93vq6xePQB5tq9WQAwo',
    authDomain: 'baking-recipe-cards.firebaseapp.com',
    databaseURL: 'https://baking-recipe-cards.firebaseio.com',
    projectId: 'baking-recipe-cards',
    storageBucket: 'baking-recipe-cards.appspot.com',
    messagingSenderId: '318416342901'
  },
  ApiUrl: 'http://localhost:3000',
  itemPerPage: 20
};
