import { TestBed, inject } from '@angular/core/testing';

import { CRecipeService } from './c-recipe.service';

describe('CRecipeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CRecipeService]
    });
  });

  it('should be created', inject([CRecipeService], (service: CRecipeService) => {
    expect(service).toBeTruthy();
  }));
});
