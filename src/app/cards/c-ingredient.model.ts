export class CIngredientModel {
    constructor(
        public name: string,
        public amount: number,
        public unit) {
    }
}

