import { TestBed, inject } from '@angular/core/testing';

import { DataFbService } from './data-fb.service';

describe('DataFbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataFbService]
    });
  });

  it('should be created', inject([DataFbService], (service: DataFbService) => {
    expect(service).toBeTruthy();
  }));
});
