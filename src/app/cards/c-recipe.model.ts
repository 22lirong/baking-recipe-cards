import { CIngredientModel } from './c-ingredient.model';

export class CRecipeModel {
    public name: string;
    public description: string;
    public imagePath: string;
    public ingredients: CIngredientModel[];

    constructor(
        name: string,
        description: string,
        imagePath: string,
        ingredients: CIngredientModel[]) {
            this.name = name;
            this.description = description;
            this.imagePath = imagePath;
            this.ingredients = ingredients;
        }
}
