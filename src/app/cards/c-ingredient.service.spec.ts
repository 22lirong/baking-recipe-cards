import { TestBed, inject } from '@angular/core/testing';

import { CIngredientService } from './c-ingredient.service';

describe('CIngredientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CIngredientService]
    });
  });

  it('should be created', inject([CIngredientService], (service: CIngredientService) => {
    expect(service).toBeTruthy();
  }));
});
