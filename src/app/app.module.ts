import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ToastyModule } from 'ng2-toasty';

import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

import { MenuModule, PanelModule, ChartModule, InputTextModule, 
         ButtonModule, InputMaskModule, InputTextareaModule, 
         EditorModule, CalendarModule, RadioButtonModule, 
         FieldsetModule, DropdownModule, MultiSelectModule, 
         ListboxModule, SpinnerModule, SliderModule, 
         RatingModule, DataTableModule, ContextMenuModule, 
         TabViewModule, DialogModule, StepsModule, 
         ScheduleModule, TreeModule, GMapModule, 
         DataGridModule, TooltipModule, ConfirmationService, 
         ConfirmDialogModule, GrowlModule, DragDropModule, 
         GalleriaModule } from 'primeng/primeng';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { RecipeItemComponent } from './recipes/recipe-list/recipe-item/recipe-item.component';
import { RecipeAddComponent } from './recipes/recipe-add/recipe-add.component';
import { RecipesComponent } from './recipes/recipes.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { RecipeService } from './recipes/recipe.service';
import { RecipeByCategoryComponent } from './recipes/recipe-by-category/recipe-by-category.component';

import { SelectItem } from 'primeng/components/common/api';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { AdminComponent } from './auth/admin/admin.component';
import { MemberService } from './auth/member.service';
import { MemberDetailComponent } from './auth/member-detail/member-detail.component';
import { MemberEditComponent } from './auth/member-edit/member-edit.component';
import { UploadComponent } from './auth/upload/upload.component';
import { MemberDeleteComponent } from './auth/member-delete/member-delete.component';

import { CardsComponent } from './cards/cards.component';
import { CardListComponent } from './cards/card-list/card-list.component';
import { CardNewComponent } from './cards/card-new/card-new.component';
import { CardDetailComponent } from './cards/card-detail/card-detail.component';
import { CardEditComponent } from './cards/card-edit/card-edit.component';
import { CardDeleteComponent } from './cards/card-delete/card-delete.component';

import { DataFbService } from './cards/data-fb.service';
import { CRecipeService } from './cards/c-recipe.service';
import { CIngredientService } from './cards/c-ingredient.service';

const appRoutes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  { path: "recipes", component: RecipesComponent },
  { path: "recipes/recipe-list", component: RecipeListComponent },
  { path: "recipes/recipe-list/recipe-item", component: RecipeItemComponent },
  { path: "recipes/recipe-detail/:recipe_id", component: RecipeDetailComponent },
  { path: "recipes/recipe-edit", component: RecipeEditComponent },
  { path: "recipes/recipe-add", component: RecipeAddComponent },
  { path: "recipes/recipe-by-category", component: RecipeByCategoryComponent },
  { path: "auth", component: AuthComponent },
  { path: "auth/register", component: RegisterComponent },
  { path: "auth/login", component: LoginComponent },
  { path: "auth/admin", component: AdminComponent },
  { path: "auth/member-detail/:id", component: MemberDetailComponent },
  { path: "auth/member-edit/:id", component: MemberEditComponent },
  { path: "auth/upload", component: UploadComponent },
  { path: "auth/member-delete/:id", component: MemberDeleteComponent },
  { path: "cards", component: CardsComponent },
  { path: "cards/card-list", component: CardListComponent },
  { path: "cards/card-new", component: CardNewComponent },
  { path: "cards/card-detail", component: CardDetailComponent },
  { path: "cards/card-edit", component: CardEditComponent },
  { path: "cards/card-delete", component: CardDeleteComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RecipeDetailComponent,
    RecipeEditComponent,
    RecipeListComponent,
    RecipeItemComponent,
    RecipeAddComponent,
    RecipesComponent,
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    RecipeByCategoryComponent,
    AdminComponent,
    MemberDetailComponent,
    MemberEditComponent,
    UploadComponent,
    MemberDeleteComponent,
    CardsComponent,
    CardListComponent,
    CardNewComponent,
    CardDetailComponent,
    CardEditComponent,
    CardDeleteComponent 
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MenuModule,
    PanelModule,
    ChartModule,
    InputTextModule,
    ButtonModule,
    InputMaskModule,
    InputTextareaModule,
    EditorModule,
    CalendarModule,
    RadioButtonModule,
    FieldsetModule,
    DropdownModule,
    MultiSelectModule,
    ListboxModule,
    SpinnerModule,
    SliderModule,
    RatingModule,
    DataTableModule,
    ContextMenuModule,
    TabViewModule,
    DialogModule,
    StepsModule,
    ScheduleModule,
    TreeModule,
    GMapModule,
    DataGridModule,
    TooltipModule,
    ConfirmDialogModule,
    GrowlModule,
    DragDropModule,
    GalleriaModule,
    ToastyModule
  ],
  providers: [ DataFbService, ConfirmationService, MessageService, CRecipeService, CIngredientService, RecipeService, MemberService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
