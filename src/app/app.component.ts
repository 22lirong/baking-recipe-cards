import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MenuItem } from "primeng/primeng";
import { Menu } from "primeng/components/menu/menu";
import { ActivatedRoute, Router } from "@angular/router";

declare var jQuery :any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, AfterViewInit {

  menuItems: MenuItem[];
  miniMenuItems: MenuItem[];

  @ViewChild('bigMenu') bigMenu : Menu;
  @ViewChild('smallMenu') smallMenu : Menu;

  constructor(private router : Router) {

  }

  ngOnInit() {

    let handleSelected = function(event) {
      let allMenus = jQuery(event.originalEvent.target).closest('ul');
      let allLinks = allMenus.find('.menu-selected');

      allLinks.removeClass("menu-selected");
      let selected = jQuery(event.originalEvent.target).closest('a');
      selected.addClass('menu-selected');
    }

    this.menuItems = [
      {label: 'Home', icon: 'fa-home', routerLink: ['/home'], command: (event) => handleSelected(event)},
      {label: 'Browse Recipes', icon: 'fa-search', routerLink: ['/recipes/recipe-list'], command: (event) => handleSelected(event)},
      {label: 'Add New Recipe', icon: 'fa-plus', routerLink: ['/recipes/recipe-add'], command: (event) => handleSelected(event)},
      {label: 'Join FREE', icon: 'fa-address-book', routerLink: ['/auth/register'], command: (event) => handleSelected(event)},
      {label: 'Sign In', icon: 'fa-user-circle', routerLink: ['/auth/login'], command: (event) => handleSelected(event)},
      {label: 'Admin', icon: 'fa-lock', routerLink: ['/auth/admin'], command: (event) => handleSelected(event)},
      {label: 'All Recipe Cards', icon: 'fa-search', routerLink: ['/cards/card-list'], command: (event) => handleSelected(event)},
      {label: 'Add New Card', icon: 'fa-plus-square', routerLink: ['/cards/card-new'], command: (event) => handleSelected(event)},
      {label: 'View Card', icon: 'fa-file', routerLink: ['/cards/card-detail'], command: (event) => handleSelected(event)},
      {label: 'Edit Card', icon: 'fa-edit', routerLink: ['/cards/card-edit'], command: (event) => handleSelected(event)},
      {label: 'Delete Card', icon: 'fa-minus-square', routerLink: ['/cards/card-delete'], command: (event) => handleSelected(event)},
    ]

    this.miniMenuItems = [];
    this.menuItems.forEach( (item : MenuItem) => {
      let miniItem = { icon: item.icon, routerLink: item.routerLink }
      this.miniMenuItems.push(miniItem);
    })

  }

  selectInitialMenuItemBasedOnUrl() {
    let path = document.location.pathname;
    let menuItem = this.menuItems.find( (item) => { return item.routerLink[0] == path });
    if (menuItem) {
      let selectedIcon = this.bigMenu.container.querySelector(`.${menuItem.icon}`);
      jQuery(selectedIcon).closest('li').addClass('menu-selected');
    }
  }

  ngAfterViewInit() {
    this.selectInitialMenuItemBasedOnUrl();
  }

}


