import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
//import { Observable } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { RecipeModel } from '../recipes/recipe-model';
//import { IngredientModel } from '../recipes/ingredient-model';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class RecipeService {
  
  recipesChanged = new Subject<RecipeModel[]>();

  private recipeRootApiUrl = `${environment.ApiUrl}/api/recipes`;

  constructor(private httpClient: HttpClient, 
              private toastyService: ToastyService, 
              private toastyConfig: ToastyConfig) {

  }

  public addRecipe (recipe: RecipeModel): Observable<RecipeModel> {
    console.log(recipe);
    console.log(this.recipeRootApiUrl);
    return this.httpClient.post<RecipeModel>(this.recipeRootApiUrl, recipe, httpOptions)
      .pipe(
        catchError(this.handleError('addRecipe', recipe))
      );
  }

  public updateRecipe (recipe: RecipeModel): Observable<RecipeModel> {
    console.log(recipe);
    console.log(this.recipeRootApiUrl);
    return this.httpClient.put<RecipeModel>(this.recipeRootApiUrl, recipe, httpOptions)
      .pipe(
        catchError(this.handleError('updateRecipe', recipe))
      );
  }

  /*
  public getAllRecipes(): Observable<Recipe[]>{
    return this.httpClient.get('api/recipes')
      .pipe(
        catchError(this.handleError('listRecipe', recipe))
      );
  }
  */
 
  /*
  public searchRecipes(model): Observable<RecipeResult[]>{
    var getURL = `${this.searchRecipeApiURL}?keyword=${model.keyword}&searchType=${model.searchType}&sortBy=${model.sortBy}&currentPerPage=${model.currentPerPage}&itemsPerPage=${model.itemsPerPage}`;
    return this.httpClient.get<RecipeResult[]>(getURL, httpOptions)
      .pipe(
        catchError(this.handleError<RecipeResult[]>('searchRecipes'))
      );
  }
  
  */
  /*
  public deleteBook (recipe: RecipeModel): Observable<RecipeModel> {
    console.log(recipe);
    console.log(recipe.id);
    console.log(this.recipeRootApiUrl);
    const deleteUrl = `${this.recipeRootApiUrl}/${recipe.id}`;
    console.log(deleteUrl);
    return this.httpClient.delete<RecipeModel>(deleteUrl, httpOptions)
      .pipe(
        catchError(this.handleError('deleteRecipe', recipe))
      );
  }
  */

  /*
  public getAllRecipes (recipe: RecipeModel): Observable<RecipeModel> {
    console.log(recipe);
    console.log(this.recipeRootApiUrl);
    //var getURL = `${this.searchRecipeApiURL}`;
    return this.httpClient.get<RecipeResult[]>(this.recipeRootApiUrl, recipe, httpOptions)
      .pipe(
        catchError(this.handleError<RecipeResult[]>('listRecipes'))
      );
  }
*/

  /*
  public searchRecipes(model): Observable<RecipeResult[]>{
    var getURL = `${this.searchRecipeApiURL}?keyword=${model.keyword}&searchType=${model.searchType}&sortBy=${model.sortBy}&currentPerPage=${model.currentPerPage}&itemsPerPage=${model.itemsPerPage}`;
    return this.httpClient.get<RecipeResult[]>(getURL, httpOptions)
      .pipe(
        catchError(this.handleError<RecipeResult[]>('searchRecipes'))
      );
  }
  */

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.addToastMessage("Error", JSON.stringify(error.error));
      return Observable.throw(error  || 'backend server error');
    };
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
        title: title,
        msg: msg,
        showClose: true,
        timeout: 3500,
        theme: 'bootstrap',
        onAdd: (toast: ToastData) => {
            console.log('Toast ' + toast.id + ' has been added!');
        },
        onRemove: function(toast: ToastData) {
            console.log('Toast ' + toast.id + ' has been removed!');
        }
    };
    this.toastyService.error(toastOptions);
  }
}
