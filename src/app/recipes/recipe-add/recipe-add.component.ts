import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators , NgForm} from "@angular/forms";
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea'
import { EditorModule } from 'primeng/editor';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { SpinnerModule } from 'primeng/spinner';
import { CheckboxModule } from 'primeng/checkbox';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FileUploadModule } from 'primeng/fileupload';

import { Observable } from 'rxjs/Observable';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

import { RecipeModel } from '../recipe-model';
import { RecipeService } from '../recipe.service';

import { IngredientModel } from '../ingredient-model'

@Component({
  selector: 'app-recipe-add',
  templateUrl: './recipe-add.component.html',
  styleUrls: ['./recipe-add.component.css']
})
export class RecipeAddComponent implements OnInit {

  @ViewChild('addRecipeForm') addRecipeForm: NgForm;

  recipesObservable: Observable<RecipeModel>;
  
  //model = new RecipeModel('','','','','',[]);
  
  model = new RecipeModel('','','','');
  
  constructor( private recipeSrv: RecipeService, 
               private toastyService: ToastyService, 
               private toastyConfig: ToastyConfig ) { }

  ngOnInit() {
  }

  onSaveRecipe(){
    console.log('form = ', this.addRecipeForm.value);
    console.log("Add new recipe!");
    console.log(JSON.stringify(this.model));
    console.log(this.model);
    this.recipesObservable = this.recipeSrv.addRecipe(this.model)
      .map(result => result);
      this.recipesObservable.subscribe(recipe => this.addToastMessage("Added recipe.", this.model.name));
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
        title: title,
        msg: msg,
        showClose: true,
        timeout: 4500,
        theme: 'bootstrap',
        onAdd: (toast: ToastData) => {
            console.log('Recipe ' + toast.id + ' has been added!');
        }
    };
    this.toastyService.success(toastOptions);
  }
}


