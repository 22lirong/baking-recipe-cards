import { IngredientModel } from './ingredient-model';

export class RecipeModel {
    
    public name: string;
    public description: string;
    public category: string;
    public instructions: string;
    //public imageURL: string;
    //public ingredients: IngredientModel[];
    
    constructor(name: string, 
                description: string, 
                category: string, 
                instructions: string
                //imageURL: string, 
                //ingredients: IngredientModel[]
    ) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.instructions = instructions;
        //this.imageURL = imageURL;
        //this.ingredients = ingredients;
    }
}

