export class IngredientModel {
    constructor(
        public amount: number,
        public unit: string,
        public name: string) {
    }
}