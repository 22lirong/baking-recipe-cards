import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MemberService } from '../member.service';
import { TableModule } from 'primeng/table';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  offset = 0;
  private limit = 10;
  
  members = [];

  constructor(private memberSvc: MemberService, 
    private router: Router, 
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    console.log('in ngOnInit')
    this.loadMembers();
  }

  private loadMembers() {
    this.memberSvc.getAllMembers({offset: this.offset, limit: this.limit})
      .then((result) => { this.members = result })
      .catch((error) => { console.error(error); });
  }

  showDetail(id: number) {
    console.log('> id: %d', id);
    this.router.navigate([ '/auth/member-detail', id]);
  }

  showEdit(id: number) {
    console.log('> id: %d', id);
    this.router.navigate([ '/auth/member-edit', id]);
  }
  
  showDelete(id: number) {
    console.log('> id: %d', id);
    this.router.navigate([ '/auth/member-delete', id]);
  }
}
