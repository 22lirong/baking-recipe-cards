import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { MemberService } from '../member.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent {

  @ViewChild('myPicField') myPicRef: ElementRef;

  constructor(private httpClient: HttpClient,
              private memberSvc: MemberService, 
              private router: Router, 
              private activatedRoute: ActivatedRoute) {
  }

  processUpload(myPicForm: NgForm, myPicField: HTMLInputElement) {
    console.log('> process: ', myPicField);
    console.log('> file name: ', myPicField.files[0]);
    console.log('> myPicRef: ', this.myPicRef.nativeElement.files[0]);

    const uploadData = new FormData();
    uploadData.set('myPic', this.myPicRef.nativeElement.files[0]);
    uploadData.set('picTitle', myPicForm.value.picTitle);

    this.httpClient.post('http://localhost:3000/upload', uploadData)
      .subscribe(
        (result) => {
          console.log('>> OK: ', result);
        }, //good
        (error) => {
          console.error('>> error: ', error);
        }, //bad
        () => {
          console.log('> observable is complete.');
        } //very good
      );
  }

  goBack() {
    this.router.navigate(['/auth/admin']);
  }
}
