import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';

import { SelectItem } from 'primeng/components/common/api';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

import { MemberModel } from '../member-model';
import { MemberService } from '../member.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import 'rxjs/operators';

import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

@Component({
  selector: 'app-member-delete',
  templateUrl: './member-delete.component.html',
  styleUrls: ['./member-delete.component.css']
})
export class MemberDeleteComponent implements OnInit {

  membersObservable: Observable<MemberModel>;

  constructor(private messageService: MessageService, 
    private memberSrv: MemberService,
    private toastyService: ToastyService, 
    private toastyConfig: ToastyConfig,
    private router: Router, 
    private activatedRoute: ActivatedRoute) { }
  
  ngOnInit() {
  }

  goBack() {
    this.router.navigate(['/auth/admin']);
  }

  onDeleteMember(id: number){
    console.log("Remove member!");
    this.memberSrv.deleteMember(id);
    //this.membersObservable = this.memberSrv.deleteMember(id)
      //.map(result => result);
    //  this.membersObservable.subscribe(member => this.addToastMessage("Removed member!", id));
    }

    addToastMessage(title, msg) {
      let toastOptions: ToastOptions = {
          title: title,
          msg: msg,
          showClose: true,
          timeout: 4500,
          theme: 'bootstrap',
          onAdd: (toast: ToastData) => {
              console.log('Book ' + toast.id + ' has been added!');
          }
      };
      this.toastyService.success(toastOptions);
    }
  }
