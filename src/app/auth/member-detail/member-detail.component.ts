import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MemberService } from '../member.service';
import { Subscription } from 'rxjs/Subscription';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';

import { SelectItem } from 'primeng/components/common/api';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})
export class MemberDetailComponent implements OnInit, OnDestroy {

  member = {};
  id = 0;

  private id$: Subscription;

  constructor(private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private memberSvc: MemberService) { }

  ngOnInit() {
    console.log("MemberDetail ngOnInit: ")

    this.id$ = this.activatedRoute.params.subscribe(
      (param) => {
        console.log('> param  = ', param);
        this.id = parseInt(param.id);
        this.memberSvc.getMember(this.id)
          .then((result) => this.member = result)
          .catch((error) => {
            alert(`Error: ${JSON.stringify(error)}`);
          });
      }
    )
  }

  ngOnDestroy(){
    this.id$.unsubscribe();
  }

  goBack() {
    this.router.navigate(['/auth/admin']);
  }

  uploadPic() {
    this.router.navigate(['/auth/upload']);
  }

  editDetail() {
    this.router.navigate(['/auth/member-edit/', this.id]);
  }

  deleteDetail() {
    this.router.navigate(['/auth/member-delete/', this.id]);
  }
}