import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

import { MemberModel } from './member-model'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class MemberService {

  readonly SERVER = 'http://localhost:3000';
  private memberRootApiUrl = `http://localhost:3000/members`;

  constructor(private httpClient: HttpClient,
              private toastyService: ToastyService,
              private toastyConfig: ToastyConfig) { }

  getAllMembers(config = {}): Promise<any> {
    let qs = new HttpParams();
    qs = qs.set('limit', config['limit'] || 20)
      .set('offset', config['offset'] || 0);
    return (
      this.httpClient.get(`${this.SERVER}/members`, {params: qs})
      .take(1).toPromise()
    );    
  }

  getMember(id: number): Promise<any> {
    return (
      this.httpClient.get(`${this.SERVER}/members/${id}`)
        .take(1).toPromise()
    );
  }

  public addMember(member: MemberModel):Observable<MemberModel> {
    console.log(member);
    return this.httpClient.post<MemberModel>(`${this.SERVER}/members`, member, httpOptions)
      .pipe(
        catchError(this.handleError('addMember', member))
      );
  }

  public updateMember(member: MemberModel):Observable<MemberModel> {
    console.log(member);
    return this.httpClient.put<MemberModel>(`${this.SERVER}/members`, member, httpOptions)
      .pipe(
        catchError(this.handleError('updateMember', member))
      );
  }

  /*
  addMember(config = {}): Promise<any> {
    let qs = new HttpParams();
    qs = qs.set('email', config['email'])
      .set('password', config['password'])
      .set('last_name', config['last_name'])
      .set('first_name', config['first_name']);
    return (
      this.httpClient.post(`${this.SERVER}/members`, {params: qs})
        .take(1).toPromise()
    );
  }
  */

  /*
  updateMember(id: number, config = {}): Promise<any> {
    let qs = new HttpParams();
    qs = qs.set('email', config['email'])
      .set('password', config['password'])
      .set('last_name', config['last_name'])
      .set('first_name', config['first_name']);
    return (
      this.httpClient.put(`${this.SERVER}/members/${id}`, {params: qs})
        .take(1).toPromise()
    );
  }
  */

  public deleteMember(id: number): Promise<any> {
    return (
      this.httpClient.delete(`${this.SERVER}/members/${id}`)
        .take(1).toPromise()
    );
  }

  private handleError<T> (operation = 'operation', result?:T){
    return (error: any): Observable<T> => {
      this.addToastMessage("Error", JSON.stringify(error.error));
      return Observable.throw(error || 'backend server error');
    };
  }

  addToastMessage(title, msg){
    let toastOptions: ToastOptions = {
      title: title,
      msg: msg,
      showClose: true,
      timeout: 3500,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: function(toast: ToastData){
        console.log('Toast ' + toast.id + ' has been removed!');
      }
    };
    this.toastyService.error(toastOptions);
    }
  }

