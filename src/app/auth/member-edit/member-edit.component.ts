import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { InputMaskModule } from 'primeng/inputmask';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea'
import { EditorModule } from 'primeng/editor';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { SpinnerModule } from 'primeng/spinner';
import { CheckboxModule } from 'primeng/checkbox';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FileUploadModule } from 'primeng/fileupload';
import { PanelModule } from 'primeng/panel';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Observable } from 'rxjs/Observable';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

import { SelectItem } from 'primeng/components/common/api';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

import { MemberModel } from '../member-model';
import { MemberService } from '../member.service';

//import { Observer } from 'rxjs/Observer';
//import 'rxjs/add/operator/map';
//import 'rxjs/Rx';
//import 'rxjs/operators';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/toPromise';

//import { Subject } from 'rxjs/Subject';
//import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {

  constructor(private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private memberSvc: MemberService,
    private toastyService: ToastyService, 
    private toastyConfig: ToastyConfig) { }


  ngOnInit() {
  }

  /*
  saveDetail() {
    this.router.navigate(['/auth/member-detail/', this.id]);
  }
  */

  uploadPic() {
  this.router.navigate(['/auth/upload']);
  }

  goBack() {
    this.router.navigate(['/auth/admin']);
  }
}
