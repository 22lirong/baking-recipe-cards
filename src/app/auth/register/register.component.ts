import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';

import { SelectItem } from 'primeng/components/common/api';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

import { MemberModel } from '../member-model';
import { MemberService } from '../member.service';

import { Observable } from 'rxjs/Observable';
//import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import 'rxjs/operators';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/toPromise';

//import { Subject } from 'rxjs/Subject';

//import { HttpClient, HttpParams } from '@angular/common/http';

import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @ViewChild(NgForm) addaddMembersForm: NgForm;

  membersObservable: Observable<MemberModel>;
  model = new MemberModel('','','','');

  msgs: Message[] = [];

  submitted: boolean;

  constructor(private messageService: MessageService, 
              private memberSrv: MemberService,
              private toastyService: ToastyService, 
              private toastyConfig: ToastyConfig) {}

  ngOnInit() {
  }
  
/*  
  onSubmit(valid: string) {
    this.submitted = true;
    //console.log(this.registerForm);
    //alert(JSON.stringify(this.registerForm.value));
    console.log(this.registerForm.value);
    this.registerForm.reset();
    this.msgs = [];
    this.msgs.push({severity:'danger', summary:'Success Message', detail:'Form submitted'});
  }
*/  
  
  onSaveMember(){
    console.log("Add new member!");
    console.log(JSON.stringify(this.model));
    console.log(this.model);
    this.membersObservable = this.memberSrv.addMember(this.model)
      .map(result => result);
      this.membersObservable.subscribe(member => this.addToastMessage("Added member!", this.model.email));
      this.msgs = [];
      this.msgs.push({severity:'danger', summary:'Success Message', detail:'Form submitted'});
    }

    addToastMessage(title, msg) {
      let toastOptions: ToastOptions = {
          title: title,
          msg: msg,
          showClose: true,
          timeout: 4500,
          theme: 'bootstrap',
          onAdd: (toast: ToastData) => {
              console.log('Book ' + toast.id + ' has been added!');
          }
      };
      this.toastyService.success(toastOptions);
    }

  //showSuccess() {
  //  this.msgs = [];
  //  this.msgs.push({severity:'success', summary:'Success Message', detail:'Form submitted'});
  //}
}
