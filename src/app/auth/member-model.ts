export class MemberModel {
    constructor(
        public email: string,
        public password: string,
        public last_name: string,
        public first_name: string
    ){}
}
