import { Component, OnInit } from '@angular/core';
import { GalleriaModule } from 'primeng/galleria';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  images: any[];
  
  constructor() { }

  ngOnInit() {

    this.images = [];
    this.images.push({source:'assets/home/img01.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img02.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img03.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img04.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img05.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img06.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img07.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img08.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img09.jpg', alt:'', title: ''});
    this.images.push({source:'assets/home/img10.jpg', alt:'', title: ''});
  }
}
